package singleton;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(Config.class);
        ctx.refresh();
        
        MyBean myBean1 = ctx.getBean(MyBean.class);
        myBean1.message = "Hello";
        myBean1.hello();
        
        myBean1.message = "Hello Again";
        MyBean myBean2 = ctx.getBean(MyBean.class);
        myBean2.hello();
        
        
        Bullet b1 = ctx.getBean(Bullet.class);
        Bullet b2 = ctx.getBean(Bullet.class);
        b1.fire();
        b2.fire();
    }
}
