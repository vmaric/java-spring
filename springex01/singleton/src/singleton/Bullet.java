package singleton;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Bullet {
    public void fire(){
        System.out.println(this);
    }
}
