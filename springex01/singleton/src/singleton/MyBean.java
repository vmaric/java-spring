package singleton;

import org.springframework.stereotype.Component;

@Component
public class MyBean {
    public String message;
    public void hello(){
        System.out.println(message);
    }
}
