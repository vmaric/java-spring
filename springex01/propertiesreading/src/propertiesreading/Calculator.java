package propertiesreading;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

 
public class Calculator {
    
    @Value( "${operation}" )
    String operation;
    @Value( "${a}" )
    int operanda;
    @Value( "${b}" )
    int operandb; 
    
    public void dowork(){
        switch(operation){
            case "+":
                System.out.println(operanda + operandb);
                break;
            case "-":
                System.out.println(operanda - operandb);
                break;
            case "*":
                System.out.println(operanda * operandb);
                break;
            case "/":
                System.out.println(operanda / operandb);
                break;
        }
    }
}
