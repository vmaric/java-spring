package propertiesreading;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("config.xml");
        Calculator calc = (Calculator)ctx.getBean("calculator");
        calc.dowork();
    }
}
