package configan;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(configan.config.MyConfig.class);
        ctx.refresh();
        MyBean mb = ctx.getBean(MyBean.class);
        mb.hello();
    }
}
