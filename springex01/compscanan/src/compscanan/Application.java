package compscanan;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(); 
        ctx.register(AppConfig.class); 
        ctx.refresh(); 
        MyBean mb = ctx.getBean(MyBean.class);
        mb.hello();
    }
}
