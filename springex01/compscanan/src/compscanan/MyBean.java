package compscanan;

import org.springframework.stereotype.Component;

@Component
public class MyBean {
    public void hello(){
        System.out.println("Hello from MyBean");
    }
}
