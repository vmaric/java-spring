package gpscoords;
public interface ICoordinateParser {
    public Point parse(String text);
}
