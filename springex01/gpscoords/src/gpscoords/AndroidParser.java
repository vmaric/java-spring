package gpscoords;
public class AndroidParser implements ICoordinateParser { 
    @Override
    public Point parse(String text) {
         String[] pt = text.split("\\|");
            return new Point(Double.valueOf(pt[0]),Double.valueOf(pt[1]));
    } 
}