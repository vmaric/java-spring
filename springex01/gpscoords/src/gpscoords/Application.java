package gpscoords; 
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext; 
public class Application {
    public static void main(String[] args) throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("config.xml"); 
        BufferedReader br = (BufferedReader)context.getBean("bReader");
        ICoordinateParser parser = (ICoordinateParser)context.getBean("coordinateParser");
        String line;
        List<Point> pts = new ArrayList<>();
        while((line=br.readLine())!=null){
            pts.add(parser.parse(line));
        }
        for(Point pt : pts){
            System.out.println("lat: " + pt.lat + " lon: " + pt.lon);
        }
    }
}
