package hellobeanxml1;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class Application {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("config.xml");
        MyInterface mc = (MyInterface)ctx.getBean("myBean");
        mc.f();
    }
}
