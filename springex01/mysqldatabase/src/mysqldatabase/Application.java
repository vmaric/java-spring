package mysqldatabase;

 
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import javax.sql.DataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) throws SQLException {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("config.xml");
        Model model = (Model)ctx.getBean("model");
        ConsoleReader consoleReader = (ConsoleReader)ctx.getBean("consolereader"); 
        while(true){
            System.out.println("1 - list all movies, 2 - movie by id, 3 - exit");
            System.out.println("Enter command:");
            switch(consoleReader.getInt()){
                case 1:
                    model.listMovies();
                    break;
                case 2:
                    System.out.println("Enter id:");
                    model.movieById(consoleReader.getInt());
                    break;
                case 3:
                    System.out.println("Bye bye");
                    System.exit(0);
                    break;
            }
        }  
        
    }
}
