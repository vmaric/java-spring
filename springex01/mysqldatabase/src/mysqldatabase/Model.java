package mysqldatabase; 

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;

public class Model {
    
    @Autowired
    DataSource dataSource;
    
    public void listMovies() throws SQLException{
            Connection conn = dataSource.getConnection(); 
            ResultSet rs = conn.createStatement().executeQuery("select film_id, title from film");
            while(rs.next()){
                System.out.println(rs.getInt("film_id") + " " + rs.getString("title"));
            }
    }
    
    public void movieById(int id) throws SQLException{
            Connection conn = dataSource.getConnection(); 
            ResultSet rs = conn.createStatement().executeQuery("select description from film where film_id = " + id);
            while(rs.next()){
                System.out.println(rs.getString("description"));
            }
    }
    
}
