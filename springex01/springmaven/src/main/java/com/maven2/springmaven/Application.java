package com.maven2.springmaven;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) { 
        ApplicationContext ctx = new ClassPathXmlApplicationContext("config.xml"); 
        Hello hello = ctx.getBean(Hello.class);
        hello.hello();
    }
}
