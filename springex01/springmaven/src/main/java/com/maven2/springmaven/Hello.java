package com.maven2.springmaven;

import org.springframework.stereotype.Component;

@Component
public class Hello {
    public void hello(){
        System.out.println("Hello World!");
    }
}
