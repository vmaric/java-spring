package autowire;

import org.springframework.stereotype.Component;

@Component("myBeanTestIdentifier")
public class MyBean { 
    public void hello(){
        System.out.println("Hello from My Bean");
    }
}

