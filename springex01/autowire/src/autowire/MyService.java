package autowire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyService {
    
    @Autowired
    MyBean myBean;
    
    @Autowired
    MyBean myBeanTestIdentifier;
    
    public void hello(){
        myBean.hello();
        myBeanTestIdentifier.hello();
    }
}
