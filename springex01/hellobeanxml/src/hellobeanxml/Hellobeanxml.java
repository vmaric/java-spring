package hellobeanxml;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class Hellobeanxml {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        MyBean mb = (MyBean)ctx.getBean("myBean");
        mb.hello(); 
    }
}
