package dependency_tipization;
public interface Vehicle {
    public String drive();
}
