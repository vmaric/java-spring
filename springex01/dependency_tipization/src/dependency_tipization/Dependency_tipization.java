package dependency_tipization;
public class Dependency_tipization {
    public static void main(String[] args) {
        Car c = new Car();
        VehicleService vehicleService = new VehicleService(c);
    }
}
