package dependency_tipization;
public class Bike implements Vehicle { 
    @Override
    public String drive() {
        return " driving a bike";
    } 
}
