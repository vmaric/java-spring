package dependency_tipization;
public class Car implements Vehicle { 
    @Override
    public String drive() {
        return " driving a car";
    } 
}
