package langpacks;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class SerbianEnglish implements LangPack {
    @Override
    public Map<String,String> getWords(){
        return new HashMap<String, String>(){
            {
                put("kuća","House");
                put("automobil","Car");
                put("škola","School");
            }  
        };
    } 
}
