package langpacks;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class EnglishSerbian implements LangPack {
    @Override
    public Map<String,String> getWords(){
        return new HashMap<String, String>(){
            {
                put("house","Kuća");
                put("car","Automobil");
                put("school","Škola");
            }  
        };
    } 
}
