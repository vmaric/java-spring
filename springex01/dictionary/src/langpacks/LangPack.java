package langpacks;

import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public interface LangPack {
    public Map<String,String> getWords();
}
