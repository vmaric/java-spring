package dictionary; 
import java.util.Scanner;
import org.springframework.stereotype.Component; 

@Component
public class UI {
    
    private Scanner scanner;  
    public Scanner getScanner() { return scanner; } 
    public void setScanner(Scanner scanner) { this.scanner = scanner; }
    
    public String getWord(){  return getScanner().nextLine(); } 
    
}
