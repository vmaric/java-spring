package dictionary;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) { 
        ApplicationContext ctx = new ClassPathXmlApplicationContext("config.xml");
        DictionaryService dict = (DictionaryService)ctx.getBean("dictionaryService");
        dict.start();
    }
}
