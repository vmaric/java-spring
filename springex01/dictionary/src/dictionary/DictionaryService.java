package dictionary;

import langpacks.LangPack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class DictionaryService {
    
    @Autowired
    private ApplicationContext appContext;  
    
    private UI ui;
    public UI getUi() {  return ui; }
    public void setUi(UI ui) { this.ui = ui; }
    
    void start(){
        while(true){
            System.out.println("From: ");
            String from = getUi().getWord();
            System.out.println("To: ");
            String to   = getUi().getWord();
            System.out.println("Word: ");
            String word = getUi().getWord(); 
            LangPack langPack = (LangPack)appContext.getBean(from + "_" + to); 
            if(langPack.getWords() == null){
                System.out.println("No such dictionary");
                return;
            }
            if(!langPack.getWords().containsKey(word.toLowerCase())){
                 System.out.println("No such word in dictionary");
                return;
            } 
            System.out.println("Translation from " + from + " to " + to + " for word " + word + " is: "); 
            System.out.println(langPack.getWords().get(word.toLowerCase())); 
        } 
    } 
}
