package parambean;
public class Square {
    double r;
    public Square(double r){
        this.r = r;
    }
    public void area(){
        System.out.println(this.r * this.r * Math.PI);
    }
}
