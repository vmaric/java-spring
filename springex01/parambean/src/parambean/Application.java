package parambean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class Application {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("config.xml");
        MyBean myBean = (MyBean)ctx.getBean("myBean");
        myBean.add();
        Square squareBean = (Square)ctx.getBean("squareBean");
        squareBean.area();
        
    }
}
