package aopmessage; 

import java.lang.annotation.Annotation;
import java.util.Arrays;

public class Application {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
         Class<Message> msgClass = Message.class;
         Message msg = msgClass.newInstance();
         Annotation[] annotations = msgClass.getAnnotations();
         System.out.println(Arrays.toString(annotations));
         for(Annotation annotation : annotations){
             if(annotation instanceof Greet){
                 msg.messageText = "Hello people!";
             } else
             if(annotation instanceof Goodbye){
                 msg.messageText = "Goodbye people!";
             }
         }
         msg.message();
    }
}
