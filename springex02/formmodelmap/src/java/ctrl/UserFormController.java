package ctrl;

import model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserFormController {
    
    @RequestMapping("/")
    public String form(){
        return "index";
    }
    
    @RequestMapping(value="/",method = RequestMethod.POST)
    public String form(ModelMap model, @ModelAttribute User user){
        model.put("message", "Hello " + user.getUsername() + 
                " with password " + user.getPassword() + "<hr>");
        return "index";
    }
    
    
    
}
