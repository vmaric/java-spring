package ctrl;

import model.ValueToConvert;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller 
public class ConverterController {
    
    @RequestMapping(value = "/")
    public String convert(){
        return "index";
    }
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String convert(ModelMap model, @ModelAttribute ValueToConvert valToConvert){
        model.put("result", valToConvert.convert());
        model.put("valToConvert",valToConvert);
        return "index";
    }
}
