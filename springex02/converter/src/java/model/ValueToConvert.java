package model;
public class ValueToConvert {
    private Double val; 
    private String to;
    public Double getVal() { return val; }
    public void setVal(double val) { this.val = val; } 
    public String getTo() { return to; } 
    public void setTo(String to) { this.to = to; } 
    public Double convert(){
        return "cm".equals(this.getTo()) ? this.val * 2.4 : this.val / 2.4;
    }   
}
