<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head> 
    <body> 
        <form method="post">
            <input type="number" name="val" value="${valToConvert.val}" /> to
            <select name="to">
                <option ${valToConvert.to=="in"?"selected":""} value="in">Inches</option>
                <option ${valToConvert.to=="cm"?"selected":""} value="cm">Centimeters</option>
            </select>
            <input type="submit" value="Convert" /> 
        </form>
        <hr>
        ${result}
    </body>
</html>
