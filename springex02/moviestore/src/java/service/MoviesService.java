package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import model.Category;
import model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = "moviesService")
public class MoviesService {
    
    @Autowired
    DataSource dataSource;
    
    public List<Movie> getMovies() throws SQLException {
        Connection conn =  dataSource.getConnection();
        List<Movie> result = new ArrayList<Movie>();
        ResultSet rs = conn.createStatement().executeQuery("select * from movies"); 
        while(rs.next()){
            result.add(new Movie(rs.getInt("id"), rs.getString("title"), rs.getDouble("price"), rs.getString("picture"), rs.getInt("category")));
        } 
        return result;
    }
    
    public List<Movie> getMovies(int category) throws SQLException{
        Connection conn =  dataSource.getConnection();
        List<Movie> result = new ArrayList<Movie>();
        PreparedStatement statement = conn.prepareStatement("select * from movies where category = ?");
        statement.setInt(1, category);
        ResultSet rs = statement.executeQuery();
        while(rs.next()){
            result.add(new Movie(rs.getInt("id"), rs.getString("title"), rs.getDouble("price"), rs.getString("picture"), rs.getInt("category")));
        } 
        return result;
    }
    
    public List<Category> getCategories() throws SQLException{
        Connection conn =  dataSource.getConnection();
        List<Category> result = new ArrayList<Category>();
        ResultSet rs = conn.createStatement().executeQuery("select id,name from categories"); 
        while(rs.next()){
            result.add(new Category(rs.getInt("id"), rs.getString("name")));
        } 
        return result;
    }
}
