package model;
public class Movie {
    private int id;
    private String title;
    private double price;
    private String picture;
    private int category;
    public Movie(){ }
    public Movie(int id, String title, double price, String picture, int category){
        this.id         = id;
        this.title      = title;
        this.price      = price;
        this.picture    = picture;
        this.category   = category;
    } 
    public int getId() { return id; }
    public void setId(int id) {  this.id = id; }
    public String getTitle() { return title; } 
    public void setTitle(String title) { this.title = title; } 
    public double getPrice() { return price; } 
    public void setPrice(double price) { this.price = price; } 
    public String getPicture() { return picture; } 
    public void setPicture(String picture) { this.picture = picture; } 
    public int getCategory() { return category; } 
    public void setCategory(int category) { this.category = category; }
}
