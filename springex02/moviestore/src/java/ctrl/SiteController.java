package ctrl;


import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import service.MoviesService;

@Controller
public class SiteController {
    
    @Autowired
    MoviesService moviesService;
    
    @RequestMapping("/")
    public String index(ModelMap model, @RequestParam(required = false) Integer category) throws SQLException {  
        model.put("categories", moviesService.getCategories()); 
        model.put("movies", category != null ? moviesService.getMovies(category) : moviesService.getMovies()); 
        return "index";
    }
}
