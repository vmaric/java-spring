package ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FormController {
    
    @RequestMapping(value = "/",method = RequestMethod.POST)
    @ResponseBody
    public String formpage(
            @RequestParam String username, 
            @RequestParam String password){ 
            return String.format("User logged in with <br>username: %s and password: %s", 
                    username,password); 
    }
    @RequestMapping(value = "/",method = RequestMethod.GET) 
    @ResponseBody
    public String formpage(){ 
            return "<form method='post'>"
                    + "Username: <input type='text' name='username' /><br>"
                    + "Password: <input type='text' name='password' /><br>"
                    + "<input type='submit' value='Login'></form>"; 
    }
    
}
