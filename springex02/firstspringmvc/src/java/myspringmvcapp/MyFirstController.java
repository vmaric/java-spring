package myspringmvcapp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/somethingelse")
public class MyFirstController {
    @RequestMapping("/myfirstpage")
    public String myPage(){
        return "myfirstview";
    }
}