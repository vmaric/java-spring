package ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CalculatorController { 
    @RequestMapping("/")
    public String index(){
        return "index";
    } 
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String index(
            ModelMap model, 
            @RequestParam(required = true) Double a, 
            @RequestParam(required = true) Double b, 
            @RequestParam(required = true) String operation){
        
        switch(operation){
            case "+":
                model.put("result", a + b);
                break;
            case "-":
                model.put("result", a - b);
                break;
            case "*":
                model.put("result", a * b);
                break;
            case "/":
                model.put("result", a / b);
                break;
        } 
        return "index";
    }
}
