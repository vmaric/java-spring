package ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainController {
    @RequestMapping(value = {"/","/bikes"})
    public String index(){
        return "index";
    }
    @RequestMapping("/best")
    public String best(){
        return "best";
    }
    @RequestMapping("/contact")
    public String contact(){
        return "contact";
    }
    @RequestMapping("/about")
    public String about(){
        return "about";
    }
}
