package ctrl;
 
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {
    @RequestMapping("/")
    @ResponseBody
    public String index(@RequestParam(required = false) Integer id){ 
        return String.format("Entered id is %d", id); 
    }
    @RequestMapping("/{id}/{cat}")
    @ResponseBody
    public String myPageWithPathParams(@PathVariable Integer id, @PathVariable Integer cat){ 
        return String.format("The id is %d and category is %d",id,cat); 
    }
}
