package com.sboot.sbootstart; 
import java.util.HashMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController; 


@RestController
public class MyBean { 
    @RequestMapping("/hello")
    public Object hello(){
        return new HashMap<String,String>(){
            {
                put("hello","World"); 
                put("how","Are you?"); 
            }
        };
    }
}
