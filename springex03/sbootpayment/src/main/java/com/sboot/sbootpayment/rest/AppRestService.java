package com.sboot.sbootpayment.rest;
 
import com.sboot.sbootpayment.model.Account;
import com.sboot.sbootpayment.model.AccountRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppRestService {
    
    @Autowired
    AccountRepository accountRepository;
    
    @RequestMapping("account")
    public Object getAccounts(){
        return accountRepository.findAll(); 
    }
    
    @RequestMapping(value = "withdraw/{id}/{amount}")
    public Object withdraw(@PathVariable("id") Integer id, @PathVariable("amount") Double amount){ 
        Optional<Account> acc = accountRepository.findById(id);
        if(acc.isPresent()){
            acc.get().balance -= amount;
            accountRepository.save(acc.get());
        }
        return accountRepository.findAll(); 
    }
    
    @RequestMapping(value = "deposit/{id}/{amount}")
    public Object deposit(@PathVariable("id") Integer id, @PathVariable("amount") Double amount){ 
        Optional<Account> acc = accountRepository.findById(id);
        if(acc.isPresent()){
            acc.get().balance += amount;
            accountRepository.save(acc.get());
        }
        return accountRepository.findAll(); 
    }
    
}
