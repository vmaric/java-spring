package com.sboot.sbooksockets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class MessageController {
    
    @Autowired
    private SimpMessagingTemplate brokerMessagingTemplate;
    
    @MessageMapping("/msg")
    public void message(String message) throws Exception {
        System.out.println(message);
        brokerMessagingTemplate.convertAndSend("/topic/msg",message);
    }
}
