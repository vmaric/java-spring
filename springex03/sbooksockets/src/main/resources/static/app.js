var stompClient = null;
function connect() {
    var socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) { 
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/msg', function (msg) {
            $("#msg").html(msg.body);
        });
    });
}  
function sendMessage(txt) {
    stompClient.send("/app/msg", {}, txt);
}
connect();